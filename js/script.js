let navbarCollapse = function () {
  if (
    document.body.scrollTop > 100 ||
    document.documentElement.scrollTop > 100
  ) {
    document.getElementById("navigation").classList.add("navbar-shrink");
  } else {
    document.getElementById("navigation").classList.remove("navbar-shrink");
  }
};
navbarCollapse();
window.onscroll = function () {
  navbarCollapse();
};
